import nltk
import spacy
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#load spacy
nlp = spacy.load("en_core_web_sm")
tokenizer = nltk.RegexpTokenizer(r'[.]{0,1}[a-zA-Z]+[1-9]*[+#]*')

# create filter fonction with stop words and lemmatize
def filter_content (text, sw):
    filtered_text = text.replace("'", ' ')
    doc = nlp(filtered_text)
    lemmatized_words = [token.lemma_ for token in doc]
    return  " ".join([w.lower() for w in lemmatized_words if w.lower() not in sw])

def tokenize(text):
    return tokenizer.tokenize(text.lower())

def filtered_by_tags(text, tags):
    text_tokenized = tokenize(text)
    text_tokenized_filtered = [w for w in text_tokenized if w in tags]
    return " ".join(text_tokenized_filtered)
    
def order_words_by_count(text):
    if text != text or text == '' or text  == None:
        return ""
    freq_dist = nltk.FreqDist(str(text).split())
    freq_tags = nltk.Counter()
    freq_tags += freq_dist
    ordered_words = []
    for item, value in freq_tags.most_common():
        ordered_words.append(item)
    return " ".join(ordered_words)

def filtered_major_tags(text, major_tags):
    if text != text or text == '' or text  == None:
        return ""
    text_major_tags = [tag for tag in text.split() if tag in major_tags]
    return " ".join(text_major_tags)

def tag_count_slice(value, intervals=[5, 20, 100, 1000]):
    if value < 0:
        return "inconsistent value"
    elif value < intervals[0]:
        return 'very small (<{})'.format(intervals[0])
    elif value < intervals[1]:
        return 'small ({}-{})'.format(intervals[0], intervals[1])
    elif value < intervals[2]:
        return 'medium ({}-{})'.format(intervals[1], intervals[2])
    elif value < intervals[3]:
        return 'big ({}-{})'.format(intervals[2], intervals[3])
    else:
        return 'very big(>{})'.format(intervals[3])

def score_tags(text, df_tag_count, results_count = 5, intervals=[5, 20, 100, 1000]):
    if text != text or text == '' or text  == None:
        return ""
    result = dict()
    for word in text.split():
        if word not in result:
            word_score = 2 * text.count(word)
            tag_score = df_tag_count[df_tag_count['tag']==word]['count'].iloc[0]
            if tag_score < intervals[0]:
                word_score += 0
            elif tag_score < intervals[1]:
                word_score += 1
            elif tag_score < intervals[2]:
                word_score += 2
            elif tag_score < intervals[3]:
                word_score += 3
            else:
                word_score += 4
            result[word] = word_score
    sorted_result = dict(sorted(result.items(), key=lambda x: x[1], reverse = True))
    if(len(sorted_result)<=results_count):
        return " ".join([tag for tag in list(sorted_result.keys())])
    else:
        return " ".join([tag for tag in list(sorted_result.keys())[:3]])     
    
def tags_scoring(result_tags, estimated_tags):
    result_tags = result_tags.split() 
    estimated_tags = estimated_tags.split() 
    i=0
    for tag in result_tags:
        if tag in estimated_tags:
            i+=1
    return 100 * i/len(result_tags)

def plot_grid_search(cv_results, grid_param_1, grid_param_2, name_param_1, name_param_2):
    # Get Test Scores Mean and std for each grid search
    scores_mean = cv_results['mean_test_score']
    scores_mean = np.array(scores_mean).reshape(len(grid_param_2),len(grid_param_1))
    fig = plt.figure(figsize=(24, 8))
    # Plot Grid search scores
    ax = fig.add_subplot(121)
    # Param1 is the X-axis, Param 2 is represented as a different curve (color line)
    for idx, val in enumerate(grid_param_2):
        ax.plot(grid_param_1, scores_mean[idx,:], '-o', label= name_param_2 + ': ' + str(val))
    ax.set_title("Grid Search Scores", fontsize=20, fontweight='bold')
    ax.set_xlabel(name_param_1, fontsize=16)
    ax.set_ylabel('CV Average Score', fontsize=16)
    ax.legend(loc="best", fontsize=15)
    ax.grid('on')
    
# Show top n keywords for each topic
def show_topics(vectorizer, lda_model, n_words=20):
    keywords = np.array(vectorizer.get_feature_names())
    topic_keywords = []
    for topic_weights in lda_model.components_:
        top_keyword_locs = (-topic_weights).argsort()[:n_words]
        topic_keywords.append(keywords.take(top_keyword_locs))
    return topic_keywords


def predict_with_naive(text, df_tag_count):
    all_tags = filtered_by_tags(text, df_tag_count['tag'].values)
    best_tags = score_tags(all_tags,df_tag_count)
    return best_tags

def predict_with_lda(text, stopwords, cv, lda_model):
    topicnames = ["Topic " + str(i) for i in range(lda_model.n_components)]
    # Topic-Keyword Matrix
    df_topic_kw= pd.DataFrame(lda_model.components_)
    # Assign Column and Index
    df_topic_kw.columns = cv.get_feature_names()
    df_topic_kw.index = topicnames
    textFiltered = filter_content(text.lower(),stopwords)
    # Vectorize transform
    cv_text = cv.transform([textFiltered])
    # LDA Transform
    topic_probability_scores = lda_model.transform(cv_text)
    dominant_topic_1 = topic_probability_scores.argsort(1)[:, -1]
    dominant_topic_2 = topic_probability_scores.argsort(1)[:, -2]
    dominant_topic_3 = topic_probability_scores.argsort(1)[:, -3]
    predicted_tags = " ".join(np.unique(np.concatenate((
        [w for w in df_topic_kw.iloc[dominant_topic_1].max().sort_values(ascending=False).index[:3]],
        [w for w in df_topic_kw.iloc[dominant_topic_2].max().sort_values(ascending=False).index[:2]],
        [w for w in df_topic_kw.iloc[dominant_topic_3].max().sort_values(ascending=False).index[:1]]))))
    return predicted_tags

def display_scree_plot(pca):
    scree = pca.explained_variance_ratio_*100
    plt.bar(np.arange(len(scree))+1, scree)
    plt.plot(np.arange(len(scree))+1, scree.cumsum(),c="red",marker='o')
    plt.xlabel("rang de l'axe d'inertie")
    plt.ylabel("pourcentage d'inertie")
    plt.title("Eboulis des valeurs propres")
    plt.show(block=False)
    

def predict_with_gbc_or_rfc(text, tfidf, words, stdScaler, pca, model):
    tfidf_text = tfidf.fit_transform([text])
    df_tfidf_text = pd.DataFrame(data = tfidf_text.toarray(),columns = tfidf.get_feature_names())
    for word in words:
        if not word in tfidf.get_feature_names():
            df_tfidf_text[word] = 0    
    for column in tfidf.get_feature_names():
        if not column in words: 
            df_tfidf_text.drop([column], axis=1, inplace=True)
    x_scaled = stdScaler.transform(df_tfidf_text)
    x_projected = pca.transform(x_scaled)
    return " ".join(model.predict(x_projected))

